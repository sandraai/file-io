package com.sandra;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static ArtistFileManager artistFileManager = new ArtistFileManager("artists.txt");
    private static Scanner scanner = new Scanner(System.in);
    private static ArrayList<String> currentlySavedArtists = artistFileManager.readFileContents();

    public static void introduceFileManager () {
        System.out.println("Welcome to the musical artist database where you can save all your favorite artists. Type 'exit' to exit.");
        System.out.println("Type 'display' to view the artists that will be saved to the database at the end of your session.");
        System.out.println("Type 'clear' to delete your new additions. Type 'fileinfo' to view the size of artists.txt where artists are stored.");
        System.out.println("The database currently contains:");
        System.out.println(String.join("\n", currentlySavedArtists));
    }

    public static void promptForArtists () {
        ArrayList<String> newPendingArtists = new ArrayList<String>();
        String entry = "";

        System.out.print("Add one of your favorite musical artists: ");
        while (!(entry = scanner.nextLine()).equals("exit")) {

            if (entry.equals("display")) {
                System.out.println(newPendingArtists);
            } else if (entry.equals("clear")) {
                newPendingArtists.clear();
            } else if (entry.equals("fileinfo")) {
                System.out.println("artists.txt currently has the size of " + artistFileManager.getFileSize() + " bytes.");
            } else if (currentlySavedArtists.contains(entry)) {
                System.out.println("That artist already exists in the database!");
            } else if (newPendingArtists.contains(entry)) {
                System.out.println("You already added that artist!");
            } else {
                newPendingArtists.add(entry);
            }
            System.out.print("Add one of your favorite musical artists: ");
        }

        artistFileManager.saveToFile(newPendingArtists);
    }

    public static void main(String[] args) {

        introduceFileManager();
        promptForArtists();

    }
}
