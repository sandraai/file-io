package com.sandra;

import java.io.*;
import java.util.ArrayList;

public class ArtistFileManager {
    private String filePath;
    private ArrayList<String> fileContentsArrayList = new ArrayList<String>();

    public ArtistFileManager (String filePath) {
        this.filePath = filePath;
    }

    public String getFileSize () {
        File file = new File(filePath);
        return Long.toString(file.length());
    }

    public ArrayList<String> readFileContents () {
        try (FileReader fileReader = new FileReader(filePath);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {

            String artist;
            while ((artist = bufferedReader.readLine()) != null) {
                fileContentsArrayList.add(artist);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Artist database is currently empty");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return fileContentsArrayList;
    }

    public void saveToFile (ArrayList<String> newArtists) {
        try (FileWriter fileWriter = new FileWriter(filePath, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

            for (String artist : newArtists) {
                bufferedWriter.write(artist + "\n");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
